<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function welcome($categoryId = null)
    {

        $categories = Category::pluck('title','id')->toArray();

        if($categoryId){

            $products = Product:: where('category_id', $categoryId )->orderBy('id' , 'desc')->paginate(9);


        }else{
            $products = Product:: orderBy('id' , 'desc')->paginate(9);

        }


        // $products = Product:: take(10)->get();

        return view('welcome', compact('products', 'categories'));
    }


    public function show (Product $product)
    
    {
        return view('product-details',  compact('product' ));
    }
}
