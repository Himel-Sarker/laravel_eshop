<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules\Unique;
use Image;
use PDF;


class ProductController extends Controller
{
    public function index()
    {
        // $products = Product::all();
        $products = Product::orderBy('id', 'desc')->get();

        return view('backend.products.index', compact('products'));
    }

    public function create()
    {


        $categories = Category:: pluck('title','id')->toArray();


        return view('backend.products.create', compact('categories'));
    }

    public function store(ProductRequest $request)
    {


        try {


            $requestData = $request->all();

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = time() . '.' . $file->getClientOriginalExtension();
                Image::make($request->file('image')) 
                ->resize (300, 200)
                ->save(storage_path() . '/app/public/products/'.$fileName);

                $requestData['image'] =$fileName;
            }

            

            $requestData['created_by']= Auth::user()->id;
            Product::create($requestData);



            // $validated = $request->validate([
            //     'title' => 'required|min:10|unique:products,title',
            //     'price'  => 'required|numeric'

            // ]);



            // Product::create([
            //     'title' =>  $request->title,
            //     'price' =>  $request->price

            // ]);



            // DB::table('products')->insert([
            //   'title' =>  $request->title,
            //     'price' =>  $request->price

            // ]);




            // Session:: flush ('message','Successflly Saved');

            // return redirect()->route ('products.index')->with('message', 'Successflly Saved') ;

            return redirect()->route('products.index')->withMessage('Successflly Saved !');
        } catch (QueryException $exception) {


            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }

    public function show( Product $product) //dependancy injection or Route Model Binding
    {

        $this->authorize('product-show', $product);

        // $product = Product:: where('id', $id)->firstOrFail();


        // $product = Product::findOrFail($product);


        // if(!$product){
        //     abort(404);
        // }



        // return view ('backend.products.show',[
        //     'product' => $product

        // ]);



        // $productName = 'T-Shirt';

        return view('backend.products.show', compact('product'));
    }

    public function edit(Product $product)

    {

          
        $this->authorize('product-edit', $product);

        $categories = Category:: pluck('title','id')->toArray();

        // $product = Product::findOrFail($id);


        return view('backend.products.edit', compact('product', 'categories'));
    }






    public function update(ProductRequest $request, Product $product)

    {
        try {


            $requestData = $request->all();

            // $product = Product::findOrFail($id);

            // $product->update([
            //     'title' => $request->title,
            //     'price'  => $request->price
            // ]);


            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = time() . '.' . $file->getClientOriginalExtension();
                Image::make($request->file('image')) 
                ->resize (300, 200)
                ->save(storage_path() . '/app/public/products/'.$fileName);

                $requestData['image'] =$fileName;
            }else{

                $requestData['image'] =$product->image;

            }

             $requestData['updated_by']= auth()->id();
             $product->update($requestData);

            return redirect()->route('products.index')->withMessage('Successflly Updated !');
            //    return view ('backend.products.edit', compact('product'));


        } catch (QueryException $exception) {

            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }





    public function destroy(Product $product)

    {



        $this->authorize('product-show', $product);

        // $product = Product::findOrFail($id);
       $product->update(['deleted_by' =>auth()->id()]);


        $product->delete();
        return redirect()->route('products.index')->withMessage('Successflly Deleted !');
    }






    public function trash()

    {


        $this->authorize('product-trash-list');


        $products = Product::onlyTrashed()->get();

        return view('backend.products.trash', compact('products'));
    }

    public function restore($id)

    {

        Product::withTrashed()
            ->where('id', $id)
            ->restore();

        return redirect()->route('products.trash')->withMessage('Successflly Restore !');
    }




    public function delete($id)

    {

        Product::withTrashed()
            ->where('id', $id)
            ->forceDelete();

        return redirect()->route('products.trash')->withMessage('Deleted Successflly !');
    }


    public function pdf()
    {
        $products = Product::latest()->get();
        $pdf = PDF::loadView('backend.products.pdf', compact('products'));
        return $pdf->download('products.pdf');
    }
}
