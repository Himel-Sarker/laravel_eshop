<?php

namespace App\Mail;

use App\Models\Product;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderCreated extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $product;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Product $product)
    {
        $this->user = $user;
        $this->product = $product;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         $user = $this->user;
         $product = $this->product;
        return $this->view('emails.order-created', compact('user', 'product'));
    }
}

