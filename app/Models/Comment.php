<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

protected $fillable = ['product_id','body', 'created_by'];

    public function commentBy()
    
    {
      return $this->belongsTo(User::class, 'created_by');
    }
}

