

<x-frontend.layouts.master>











<h3 style=" font-weight: bold; padding-bottom:5px; color: blue;"> &nbsp; DOCTOR LIST : </h3>
<br>



<div class="container" style="padding-right:350px;">
<!-- <div class="bg-light p-5 rounded" > -->


<div class="row row-cols-1 row-cols-md-3 mb-3 text-center">


    @foreach ($products as $product) 
   
<div class="col" >
        <div class="card mb-4 rounded-3 shadow-sm">

        
          <div class="card-header py-3">
          <img src="{{asset('storage/products/'.$product->image)}}" />
          </div>


          <div class="card-body">
            <h4 class="card-title pricing-card-title">
                
             <a href="{{ route ('single-product', $product->id)  }}"> {{ Str::limit($product->title, 50)}}</a>

            </h4>
             
            <p>Tk.{{ $product->price }}</p><br>
 
            <button type="button" class="w-100 btn btn-lg btn-primary">Add To Cart</button>        
        </div>
    </div>
    

    
</div>

@endforeach 
{{ $products->links() }}

</div> 

</div>






<br>
<br>
<br>

<div>
    <h3 style="font-weight: bold;text-align:center;color:#0000ff;margin-top:5px;margin-bottom:5px;;">SERVICES:</h3>
    <br>
    <div class="container">
        <div class="card-group">
            <div class="card">
                <img src="{{asset('frontend/pic/doc2.jpg')}}" class="card-img-top" alt="..." width="100%">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                        additional content. This content is a little bit longer.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
            <div class="card">
                <img src="{{asset('frontend/pic/Service2.jpg')}}" class="card-img-top" alt="..." width="100%">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This card has supporting text below as a natural lead-in to additional
                        content.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>
</div> 



</x-frontend.layouts.master>
