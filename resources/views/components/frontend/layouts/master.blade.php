<!DOCTYPE html>
<html lang="en">

<head>


    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Home Page</title>
    <link rel="icon" type="image/x-icon" href="{{asset('frontend/pic/logop.png')}}">



  <!-- Fontawesome min.CSS -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/all.min.css"
        integrity="sha512-10/jx2EXwxxWqCLX/hHth/vu2KY3jCF70dCQB8TSgNjbCVAC/8vai53GfMDrO2Emgwccf2pJqxct9ehpzG+MTw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/regular.min.css"
        integrity="sha512-op8gZY0YqKAatlnYvmUvSqK4sBJQWYA0APRKExBXaTR1SyHHY/Pw4vtfw+L5VMXbVQb/Xvz4xE5d5U3E0wLz/g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />


    <!-- Load Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
        integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
        </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
        integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous">
        </script>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">




    <!-- Custom CSS -->

    <link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
    <link rel="stylesheet" href="http://meyerweb.com/eric/tools/css/reset/reset.css">



<style>


/* Carousel design */


.carousel .carousel-item {
  height: 760px;
}

.carousel-item img {
    position: absolute;
    object-fit:cover;
    top: 0;
    left: 0;
    min-height: 700px;
}



</style>



</head>

<body>

    <br>



    <x-frontend.layouts.partials.header/>



      <!--  this the Carousel -->

      <section>

        <!-- Carousel // 1920 * 540 -->
        <div id="demo" class="carousel slide" data-bs-ride="carousel">

          <!-- Indicators/dots -->
          <div class="carousel-indicators">
            <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
            <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
            <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
          </div>

          <!-- The slideshow/carousel -->
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="{{asset('frontend/pic/carsoul4.jpg')}}" alt="Front picture" class="d-block" style="width:100%">
              <div class="carousel-caption">
                <h3> Journey Start From Here </h3>
                <p>This is our memory bank!</p>
              </div>
            </div>
            <div class="carousel-item">
              <img src="{{asset('frontend/pic/carsoul5.jpg')}}" alt="Full Picture" class="d-block" style="width:100%">
              <div class="carousel-caption">
                <h3> Operation Theater room of Hospital </h3>
                <p>Thank you!</p>
              </div>
            </div>
            <div class="carousel-item">
              <img src="{{asset('frontend/pic/carsoul6.jpg')}}" alt="Logo" class="d-block" style="width:100%">
              <div class="carousel-caption">
                <h3>Inside the Hospital</h3>
                <p>This Hospital has a Instrument or Tools!!</p>
              </div>
            </div>
          </div>

          <!-- Left and right controls/icons -->
          <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
            <span class="carousel-control-next-icon"></span>
          </button>
        </div>

      </section>


     <hr>
     <br>




    <main class="container">




    {{$slot}}


    

    </main>



    <br>
    <br>
    <br>


    <x-frontend.layouts.partials.footer/>

   


    <!-- Back to top button -->
    <button type="button" class="btn btn-btn btn-success btn-lg" id="btn-back-to-top">
        <i class="fas fa-arrow-up"></i>
    </button>

    <!--  Bootstrap Bundle with Popper -->

    <script type="text/javascript " src="{{asset('frontend/js/bootstrap.bundle.min.js')}}"></script>


    <!-- Custom JavaScript link and CDN -->

    <script src="{{asset('frontend/js/Srcipt.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>


</body>

</html>