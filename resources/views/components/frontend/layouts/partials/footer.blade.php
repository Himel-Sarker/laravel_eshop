<footer class="page-footer" style="background: url('frontend/pic/footerback.jpg'); background-size:cover; background-position: 0 100%; 
        box-shadow: inset 0 0 0 215px rgba(0, 0, 0, 0.48);">
        <div class="page-footer-content fixed-width"> <br>
            <small class="copyright">© Copyright 2022 - HSM. All Rights Reserved</small>


            <small class="credits">Site Designed by <a href="#"><span style="color:rgb(235, 210, 193)">Himel
                        Sarker </span></a></small>
            <br><br><br>

            <h5 class="text-5 text-transform-none font-weight-semibold text-color-light mb-4">Contact Us</h5>
            <div class="contact_us">
                <p>House #16, Road # 2, Dhanmondi R/A, Dhaka-1205, Bangladesh<br><br>
                    Phone : 09613 787801, 09666 787801<br><br>
                    E-mail :&nbsp;info@examplehospital.com</p>
            </div>

        </div>

        </div>
    </footer>
