<div class="row">

    <div id="sp-top" class="col-lg">
        <div class="sp-column text-center text-lg-left">

            <ul class="social-icons">
                <li class="social-icon-facebook">
                    <a target="_blank" href="" aria-label="facebook">
                        <span class="fab fa-facebook" style="font-size:20px;" aria-hidden="true"></span>
                    </a>
                </li>
                <li class="social-icon-twitter">
                    <a target="_blank" href="" aria-label="twitter">
                        <span class="fab fa-twitter" style="font-size:20px;" aria-hidden="true"></span>
                    </a>
                </li>
                <li>
                    <a target="_blank" href="" aria-label="Youtube">
                        <span class="fab fa-youtube" style="font-size:20px" aria-hidden="true"></span>
                    </a>
                </li>
                <li class="social-icon-linkedin">
                    <a target="_blank" href="" aria-label="LinkedIn">
                        <span class="fab fa-linkedin-in" style="font-size:20px" aria-hidden="true"></span></a>
                </li>
                <li class="social-icon-instagram">
                    <a target="_blank" href="" aria-label="Instagram">
                        <span class="fab fa-instagram" style="font-size:20px" aria-hidden="true"></span>
                    </a>
                </li>
            </ul>

        </div>
    </div>

    <div id="sp-top" class="col-lg">
        <div class="sp-column text-center text-lg-right">

            <ul class="sp-contact-info">
                <li class="sp-contact-phone">
                    <span class="fas fa-phone-alt" style="font-size:20px" aria-hidden="true"></span>
                    <a href=" "> 09666414111</a>
                </li>
                <li class="sp-contact-email "> <span class="fa fa-envelope " style="font-size:20px; " aria-hidden="true"></span>
                    <a href=" "> info@-hospital.com</a>
                </li>
                <li class="sp-contact-time ">
                    <span class="fas fa-clock " style="font-size:20px; " aria-hidden="true "></span> 24/7 : 24 Hours


                </li>
            </ul>

        </div>
    </div>

</div>





<nav class="navbar sticky-top navbar-expand-lg navbar-light" style="background-color:#89c2c9">
    <div class=" container-fluid">
        <a class="navbar-brand" href="#"><img src="{{asset('frontend/pic/logop.png')}}" alt="our logo" height="50px" width="65px;"></a>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">

                <li class="nav-item">
                    <a class="btn btn-sm btn-out-success" type="button" href="adminlogin.html">About Us</a>

                </li>

                <li class="nav-item">
                    <a class="btn btn-sm btn-out-success" type="button" href="adminlogin.html">Admin Panel</a>

                </li>

                <li class="nav-item">
                    <a class="btn btn-sm btn-out-success" type="button" href="Dcotor_login_form.html">Doctor
                        Panel</a>

                </li>

                <li class="nav-item">
                    <a class="btn btn-sm btn-out-success" type="button" href="Find_a_doctor.html">Find Doctor</a>

                </li>



                <li class="nav-item">
                    <a class="btn btn-sm btn-out-success" type="button" href="adminlogin.html">Contact Us</a>

                </li>



                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Dropdown
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">



                    @foreach ($categories as $category)

                    <li><a class="dropdown-item" href="{{route('welcome', $category->id)}}">{{$category->title}}</a></li>

                    @endforeach

                      
                    </ul>
                </li>
            


                <li class="nav-item">
                    <a class="btn btn-sm btn-success" type="button" href="Dcotor_login_form.html">Make an
                        Apointment</a>

                </li>


                <!-- <form class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form> -->




            </ul>



            <li id="heart-trigger" class="heart heart-trigger"><span class="fa fa-heart"></span></li>
            <ul>



                <a class="btn btn-sm btn-success" type="button" href="{{ route ('login') }}">Login</a>



                <a class="btn btn-sm btn-success" type="button" href="{{ route ('register') }} ">Register</a>




            </ul>

        </div>
    </div>

</nav>