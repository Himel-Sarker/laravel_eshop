

<x-backend.layouts.master>
    <div class="container-fluid px-4">
        <h1 class="mt-4">Products</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Products</li>
        </ol>

        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
              Product List


              <a class="btn btn-sm btn-success" href="{{ route('products.pdf') }}">PDF </a>



            @can('product-trash-list')
  
          
               <a class="btn btn-sm btn-info" href="{{ route('products.trash') }}">Trash list </a>
          
             @endcan




              <a class="btn btn-sm btn-primary" href="{{ route('products.create') }}">Add New</a>

            </div>
        
            <div class="card-body">

             @if(session('message'))
             <p class="alert alert-success">{{session('message')}}</p>
             @endif

                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Sl</th>
                            <th>Title</th>
                            <th>Price</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                 
                    <tbody>

                    <tbody>

                        @foreach($products as $product)
                        <tr>

                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $product->title }}</td>
                            <td>Tk. {{ $product->price }}</td>
                            <td>{{ $product->category->title ?? 'N/A' }}</td>

                            <td>
                               

                            @can('product-show', $product)

                                <a class="btn btn-info btn-sm" href="{{route('products.show',
                                    ['product'=> $product->id])}}">Show</a>

                            @endcan


                            @can('product-edit', $product)

                                <a class="btn btn-warning btn-sm" href="{{route('products.edit',
                                    ['product'=> $product->id])}}">Edit</a>
                               
                             @endcan


                             @can('product-delete', $product)

                                <form action="{{route('products.destroy',['product'=> $product->id])}}" method="POST"
                                  style="display:inline">
                                  @csrf 
                                 @method('delete')
                                 <button type= "submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want delete?')">Delete</button>

                                </form>

                             @endcan


                            </td>

                        </tr>

                        @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-backend.layouts.master>